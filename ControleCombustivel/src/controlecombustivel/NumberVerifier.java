package controlecombustivel;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JTextField;

/**
 *
 * @author mateus.silva
 */
public class NumberVerifier extends InputVerifier {

    @Override
    public boolean verify(JComponent valor) {

        String valorDoCampo = ((JTextField) valor).getText();
        try {
            Double.parseDouble(valorDoCampo);
        } catch (NumberFormatException e) {
            return false;
        }

        return true;
    }
}
